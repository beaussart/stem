use ::actix::prelude::*;
use ::actix::io::{FramedWrite, WriteHandler};
use bytes::BytesMut;
use log::{debug, info};
use tokio::codec::FramedRead;
use tokio::io::{AsyncRead, WriteHalf};
use tokio::net::tcp::TcpStream;

use std::io;
use std::net::SocketAddr;

use crate::actor::client::{ClientPackage, ClientStatus};
use crate::actor::server::{NewTCPConnection, Server};
use crate::util::codec::{Package, Codec};

pub struct TCPClient {
    pub socket_addr: SocketAddr,
    pub server: Addr<Server>,
    pub framed: FramedWrite<WriteHalf<TcpStream>, Codec>,
    pub last_ping_content: Option<BytesMut>,
}

impl TCPClient {
    pub fn new(connection: NewTCPConnection, server: Addr<Server>) -> Addr<TCPClient> {
        info!("New TCP connection: {:?}", connection.1);
        TCPClient::create(move |ctx| {
            let (r, w) = connection.0.split();
            TCPClient::add_stream(FramedRead::new(r, Codec), ctx);
            TCPClient {
                socket_addr: connection.1,
                server,
                framed: actix::io::FramedWrite::new(w, Codec, ctx),
                last_ping_content: None,
            }
        })
    }
}

impl Actor for TCPClient {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Context<Self>) {
        self.server.do_send(ClientStatus::Started(ctx.address().recipient()));
    }

    fn stopping(&mut self, ctx: &mut Context<Self>) -> Running {
        info!("Disconnected (TCP): {:?}", self.socket_addr);
        self.server.do_send(ClientStatus::Stopped(ctx.address().recipient()));
        Running::Stop
    }
}

impl WriteHandler<io::Error> for TCPClient {}

// Receive a package from remote client and transfer the package to the Server
impl StreamHandler<Package, io::Error> for TCPClient {
    fn handle(&mut self, package: Package, ctx: &mut Self::Context) {
        debug!("Incoming TCP package (from {:?}): {:?}.", self.socket_addr, package);
        match package {
            Package::Ping(content) => {
                let pong = Package::Pong(content);
                debug!("Outgoing TCP package (to {:?}): {:?}", self.socket_addr, pong);
                self.framed.write(pong);
            },
            Package::Pong(content) => {
                match self.last_ping_content {
                    // check if the pong corresponds to the ping
                    Some(ref data) => {
                        if data == &content {
                            self.last_ping_content = None;
                        } else {
                            debug!("One client stopped for not responding: {:?}", self.socket_addr);
                            ctx.stop();
                        }
                    }
                    // well, this pong came from nowhere and we don't know what to do
                    None => {}
                }
            }
            _ => self.server.do_send(ClientPackage { address: ctx.address().recipient(), package })
        }
    }
}

// Receive a package from Server and transfer the package to remote client
impl Handler<Package> for TCPClient {
    type Result = ();

    fn handle(&mut self, pkg: Package, context: &mut Self::Context) {
        match pkg {
            Package::Ping(ref content) => {
                match self.last_ping_content {
                    // if there are no problems, store the ping content for later
                    None => self.last_ping_content = Some(content.clone()),
                    // stop this actor if we have not received any pongs since last ping
                    Some(..) => {
                        debug!("One client stopped for not responding: {:?}", self.socket_addr);
                        context.stop()
                    }
                }
                self.framed.write(pkg);
            }
            Package::Message(..) => {
                debug!("Outgoing TCP package (to {:?}): {:?}", self.socket_addr, pkg);
                self.framed.write(pkg);
            }
            _ => { }
        }
    }
}
