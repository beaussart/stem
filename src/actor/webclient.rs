use ::actix::prelude::*;
use ::actix_web::{ws, Binary};
use bytes::BytesMut;
use log::{debug, error, info};
use tokio::codec::{Decoder, Encoder};

use std::net::SocketAddr;

use crate::actor::client::{ClientPackage, ClientStatus};
use crate::actor::server::Server;
use crate::util::codec::{Codec, Package};
use crate::util::webappstate::WebAppState;

pub struct WebClient {
    pub socket_addr: SocketAddr,
    pub server: Addr<Server>,
    codec: Codec,
    pub last_ping_content: Option<BytesMut>,
}

impl WebClient {
    pub fn new(addr: SocketAddr, server: Addr<Server>) -> WebClient {
        info!("New WS connection: {:?}", addr);
        WebClient {
            socket_addr: addr,
            server,
            codec: Codec,
            last_ping_content: None,
        }
    }

    fn decode(&mut self, bin: Binary) -> Option<Package> {
        let package = self.codec.decode(&mut BytesMut::from(bin.as_ref()));
        match package {
            Ok(option) => match option {
                None => {
                    error!("Malformed WS package from {:?}!", self.socket_addr);
                    None
                }
                _ => option
            }
            Err(..) => {
                error!("Malformed WS package from {:?}!", self.socket_addr);
                None
            }
        }
    }

    fn encode(&mut self, package: Package) -> Option<BytesMut> {
        let mut bytes = BytesMut::new();
        match self.codec.encode(package, &mut bytes) {
            Ok(_) => Some(bytes),
            Err(error) => {
                error!("Some problems when encoding package for WS socket {:?}! {:?}", self.socket_addr, error);
                None
            }
        }
    }

    fn send_package(&mut self, package: Package, context: &mut ws::WebsocketContext<Self, WebAppState>) {
        match self.encode(package) {
            Some(data) => {
                debug!("Outgoing WS package (to {:?}): {:?}", self.socket_addr, data);
                context.binary(data);
            }
            None => {}
        }
    }
}

impl Actor for WebClient {
    type Context = ws::WebsocketContext<Self, WebAppState>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.server.do_send(ClientStatus::Started(ctx.address().recipient()));
    }

    fn stopping(&mut self, ctx: &mut Self::Context) -> Running {
        info!("Disconnected (WS): {:?}", self.socket_addr);
        self.server.do_send(ClientStatus::Stopped(ctx.address().recipient()));
        Running::Stop
    }
}

// Receive a package from remote client and transfer the package to the Server
impl StreamHandler<ws::Message, ws::ProtocolError> for WebClient {
    fn handle(&mut self, msg: ws::Message, ctx: &mut Self::Context) {
        match msg {
            ws::Message::Ping(msg) => ctx.pong(&msg),
            ws::Message::Text(text) => ctx.text(text),
            ws::Message::Binary(bin) => {
                debug!("Incoming WS package (from {:?}): {:?}.", self.socket_addr, bin);
                match self.decode(bin) {
                    Some(package) => {
                        match package {
                            Package::Ping(content) => {
                                self.send_package(Package::Pong(content), ctx);
                            }
                            Package::Pong(content) => {
                                match self.last_ping_content {
                                    // check if the pong corresponds to the ping
                                    Some(ref data) => {
                                        if data == &content {
                                            self.last_ping_content = None;
                                        } else {
                                            debug!("One client stopped for not responding: {:?}", self.socket_addr);
                                            ctx.stop();
                                        }
                                    }
                                    // well, this pong came from nowhere and we don't know what to do
                                    None => {}
                                }
                            }
                            _ => self.server.do_send(ClientPackage { address: ctx.address().recipient(), package })
                        }
                    }
                    _ => {}
                }
            },
            _ => (),
        }
    }
}

// Receive a package from Server and transfer the package to remote client
impl Handler<Package> for WebClient {
    type Result = ();

    fn handle(&mut self, pkg: Package, ctx: &mut Self::Context) {
        match pkg {
            Package::Ping(ref content) => {
                match self.last_ping_content {
                    // if there are no problems, store the ping content for later
                    None => self.last_ping_content = Some(content.clone()),
                    // stop this actor if we have not received any pongs since last ping
                    Some(..) => {
                        debug!("One client stopped for not responding: {:?}", self.socket_addr);
                        ctx.stop()
                    }
                }
                self.send_package(pkg, ctx);
            }
            Package::Message(..) => {
                self.send_package(pkg, ctx);
            }
            _ => {}
        }
    }
}
