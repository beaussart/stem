// STEM web interface
// 2019, MIT (c) Totoro

// ui
// --------------------------------------------------------------------------------- //
var channel = document.getElementById("channel");
var join = document.getElementById("join");

function joinChannel() {
  location.href = 'channel?id=' + channel.value;
}

channel.addEventListener("keyup", function(event) {
  if (event.key == 'Enter') {
    joinChannel();
  }
});

join.addEventListener("click", joinChannel);
